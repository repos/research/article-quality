import re

from datetime import datetime

from article_quality.util import python_to_sql_list


MEDIA_PREFIXES = ['File', 'Image', 'Media']
CAT_PREFIXES = ['Category']

MEDIA_ALIASES = {
    "ab": ["Медиа", "Файл", "Афаил", "Амедиа", "Изображение"],
    "ace": ["Beureukaih", "Gambar", "Alat", "Berkas"],
    "ady": ["Медиа"],
    "af": ["Lêer", "Beeld"],
    "als": ["Medium", "Datei", "Bild"],
    "am": ["ፋይል", "ስዕል"],
    "an": ["Imachen", "Imagen"],
    "ang": ["Ymele", "Biliþ"],
    "ar": ["ميديا", "صورة", "وسائط", "ملف"],
    "arc": ["ܠܦܦܐ", "ܡܝܕܝܐ"],
    "arz": ["ميديا", "صورة", "وسائط", "ملف"],
    "as": ["চিত্ৰ", "चित्र", "চিত্র", "মাধ্যম"],
    "ast": ["Imaxen", "Ficheru", "Imaxe", "Archivu", "Imagen", "Medios"],
    "atj": ["Tipatcimoctakewin", "Natisinahikaniwoc"],
    "av": ["Медиа", "Файл", "Изображение"],
    "ay": ["Medio", "Archivo", "Imagen"],
    "az": ["Mediya", "Şəkil", "Fayl"],
    "azb": ["رسانه", "تصویر", "مدیا", "فایل", "رسانه‌ای"],
    "ba": ["Медиа", "Рәсем", "Файл", "Изображение"],
    "bar": ["Medium", "Datei", "Bild"],
    "bat-smg": ["Vaizdas", "Medėjė", "Abruozdielis"],
    "bcl": ["Medio", "Ladawan"],
    "be": ["Мультымедыя", "Файл", "Выява"],
    "be-x-old": ["Мэдыя", "Файл", "Выява"],
    "bg": ["Медия", "Файл", "Картинка"],
    "bh": ["मीडिया", "चित्र"],
    "bjn": ["Barakas", "Gambar", "Berkas"],
    "bm": ["Média", "Fichier"],
    "bn": ["চিত্র", "মিডিয়া"],
    "bpy": ["ছবি", "মিডিয়া"],
    "br": ["Skeudenn", "Restr"],
    "bs": ["Mediji", "Slika", "Datoteka", "Medija"],
    "bug": ["Gambar", "Berkas"],
    "bxr": ["Файл", "Меди", "Изображение"],
    "ca": ["Fitxer", "Imatge"],
    "cbk-zam": ["Medio", "Archivo", "Imagen"],
    "cdo": ["文件", "媒體", "圖像", "檔案"],
    "ce": ["Хlум", "Медиа", "Сурт", "Файл", "Медйа", "Изображение"],
    "ceb": ["Payl", "Medya", "Imahen"],
    "ch": ["Litratu"],
    "ckb": ["میدیا", "پەڕگە"],
    "co": ["Immagine"],
    "crh": ["Медиа", "Resim", "Файл", "Fayl", "Ресим"],
    "cs": ["Soubor", "Média", "Obrázok"],
    "csb": ["Òbrôzk", "Grafika"],
    "cu": ["Видъ", "Ви́дъ", "Дѣло", "Срѣдьства"],
    "cv": ["Медиа", "Ӳкерчĕк", "Изображение"],
    "cy": ["Delwedd"],
    "da": ["Billede", "Fil"],
    "de": ["Medium", "Datei", "Bild"],
    "din": ["Ciɛl", "Apamduööt"],
    "diq": ["Medya", "Dosya"],
    "dsb": ["Wobraz", "Dataja", "Bild", "Medija"],
    "dty": ["चित्र", "मिडिया"],
    "dv": ["ފައިލު", "މީޑިއާ", "ފައިލް"],
    "el": ["Εικόνα", "Αρχείο", "Μέσο", "Μέσον"],
    "eml": ["Immagine"],
    "eo": ["Dosiero", "Aŭdvidaĵo"],
    "es": ["Medio", "Archivo", "Imagen"],
    "et": ["Pilt", "Fail", "Meedia"],
    "eu": ["Irudi", "Fitxategi"],
    "ext": ["Archivu", "Imagen", "Mediu"],
    "fa": ["رسانه", "تصویر", "مدیا", "پرونده", "رسانه‌ای"],
    "ff": ["Média", "Fichier"],
    "fi": ["Kuva", "Tiedosto"],
    "fiu-vro": ["Pilt", "Meediä"],
    "fo": ["Miðil", "Mynd"],
    "fr": ["Média", "Fichier"],
    "frp": ["Émâge", "Fichiér", "Mèdia"],
    "frr": ["Medium", "Datei", "Bild"],
    "fur": ["Immagine", "Figure"],
    "fy": ["Ofbyld"],
    "ga": ["Íomhá", "Meán"],
    "gag": ["Mediya", "Medya", "Resim", "Dosya", "Dosye"],
    "gan": ["媒体文件", "文件", "文檔", "档案", "媒體", "图像", "圖像", "媒体", "檔案"],
    "gd": ["Faidhle", "Meadhan"],
    "gl": ["Imaxe", "Ficheiro", "Arquivo", "Imagem"],
    "glk": ["رسانه", "تصویر", "پرونده", "فاىل", "رسانه‌ای", "مديا"],
    "gn": ["Medio", "Imagen", "Ta'ãnga"],
    "gom": ["माध्यम", "मिडिया", "फायल"],
    "gor": ["Gambar", "Berkas"],
    "got": ["𐍆𐌴𐌹𐌻𐌰"],
    "gu": ["દ્રશ્ય-શ્રાવ્ય (મિડિયા)", "દ્રશ્ય-શ્રાવ્ય_(મિડિયા)", "ચિત્ર"],
    "gv": ["Coadan", "Meanyn"],
    "hak": ["文件", "媒體", "圖像", "檔案"],
    "haw": ["Kiʻi", "Waihona", "Pāpaho"],
    "he": ["תמונה", "קו", "מדיה", "קובץ"],
    "hi": ["मीडिया", "चित्र"],
    "hif": ["file", "saadhan"],
    "hr": ["Mediji", "DT", "Slika", "F", "Datoteka"],
    "hsb": ["Wobraz", "Dataja", "Bild"],
    "ht": ["Imaj", "Fichye", "Medya"],
    "hu": ["Kép", "Fájl", "Média"],
    "hy": ["Պատկեր", "Մեդիա"],
    "ia": ["Imagine", "Multimedia"],
    "id": ["Gambar", "Berkas"],
    "ig": ["Nká", "Midia", "Usòrò", "Ákwúkwó orünotu", "Ákwúkwó_orünotu"],
    "ii": ["媒体文件", "文件", "档案", "图像", "媒体"],
    "ilo": ["Midia", "Papeles"],
    "inh": ["Медиа", "Файл", "Изображение"],
    "io": ["Imajo", "Arkivo"],
    "is": ["Miðill", "Mynd"],
    "it": ["Immagine"],
    "ja": ["メディア", "ファイル", "画像"],
    "jbo": ["velsku", "datnyvei"],
    "jv": ["Barkas", "Medhia", "Gambar", "Médhia"],
    "ka": ["მედია", "სურათი", "ფაილი"],
    "kaa": ["Swret", "Таспа", "سۋرەت", "Taspa", "Su'wret", "Сурет", "تاسپا"],
    "kab": ["Tugna"],
    "kbd": ["Медиа", "Файл"],
    "kbp": ["Média", "Fichier"],
    "kg": ["Fisye"],
    "kk": ["Swret", "سۋرەت", "Таспа", "Taspa", "Сурет", "تاسپا"],
    "kl": ["Billede", "Fiileq", "Fil"],
    "km": ["ឯកសារ", "រូបភាព", "មេឌា", "មីឌា"],
    "kn": ["ಚಿತ್ರ", "ಮೀಡಿಯ"],
    "ko": ["미디어", "파일", "그림"],
    "koi": ["Медиа", "Файл", "Изображение"],
    "krc": ["Медиа", "Файл", "Изображение"],
    "ks": ["میڈیا", "فَیِل"],
    "ksh": ["Beld", "Meedije", "Medie", "Belld", "Medium", "Datei", "Meedijum", "Bild"],
    "ku": ["میدیا", "پەڕگە", "Medya", "Wêne"],
    "kv": ["Медиа", "Файл", "Изображение"],
    "kw": ["Restren"],
    "ky": ["Медиа", "Файл"],
    "la": ["Imago", "Fasciculus"],
    "lad": ["Dossia", "Medya", "Archivo", "Dosya", "Imagen", "Meddia"],
    "lb": ["Fichier", "Bild"],
    "lbe": ["Медиа", "Сурат", "Изображение"],
    "lez": ["Медиа", "Mediya", "Файл", "Şəkil", "Изображение"],
    "lfn": ["Fix"],
    "li": ["Afbeelding", "Plaetje", "Aafbeilding"],
    "lij": ["Immaggine", "Immagine"],
    "lmo": ["Immagine", "Imàjine", "Archivi"],
    "ln": ["Média", "Fichier"],
    "lo": ["ສື່ອ", "ສື່", "ຮູບ"],
    "lrc": ["رسانه", "تصویر", "رسانه‌ای", "جانیا", "أسگ", "ڤارئسگأر"],
    "lt": ["Vaizdas", "Medija"],
    "ltg": ["Medeja", "Fails"],
    "lv": ["Attēls"],
    "mai": ["मेडिया", "फाइल"],
    "map-bms": ["Barkas", "Medhia", "Gambar", "Médhia"],
    "mdf": ["Медиа", "Няйф", "Изображение"],
    "mg": ["Rakitra", "Sary", "Média"],
    "mhr": ["Медиа", "Файл", "Изображение"],
    "min": ["Gambar", "Berkas"],
    "mk": ["Податотека", "Медија", "Медиум", "Слика"],
    "ml": ["പ്രമാണം", "ചി", "മീഡിയ", "പ്ര", "ചിത്രം"],
    "mn": ["Медиа", "Файл", "Зураг"],
    "mr": ["चित्र", "मिडिया"],
    "mrj": ["Медиа", "Файл", "Изображение"],
    "ms": ["Fail", "Imej"],
    "mt": ["Midja", "Medja", "Stampa"],
    "mwl": ["Multimédia", "Fexeiro", "Ficheiro", "Arquivo", "Imagem"],
    "my": ["ဖိုင်", "မီဒီယာ"],
    "myv": ["Медия", "Артовкс", "Изображение"],
    "mzn": ["رسانه", "تصویر", "مه‌دیا", "مدیا", "پرونده", "رسانه‌ای"],
    "nah": ["Mēdiatl", "Īxiptli", "Imagen"],
    "nap": ["Fiùra", "Immagine"],
    "nds": ["Datei", "Bild"],
    "nds-nl": ["Ofbeelding", "Afbeelding", "Bestaand"],
    "ne": ["मीडिया", "चित्र"],
    "new": ["किपा", "माध्यम"],
    "nl": ["Bestand", "Afbeelding"],
    "nn": ["Fil", "Bilde", "Filpeikar"],
    "no": ["Fil", "Medium", "Bilde"],
    "nov": [],
    "nrm": ["Média", "Fichier"],
    "nso": ["Seswantšho"],
    "nv": ["Eʼelyaaígíí"],
    "oc": ["Imatge", "Fichièr", "Mèdia"],
    "olo": ["Kuva", "Medii", "Failu"],
    "or": ["ମାଧ୍ୟମ", "ଫାଇଲ"],
    "os": ["Ныв", "Медиа", "Файл", "Изображение"],
    "pa": ["ਤਸਵੀਰ", "ਮੀਡੀਆ"],
    "pcd": ["Média", "Fichier"],
    "pdc": ["Medium", "Datei", "Bild", "Feil"],
    "pfl": ["Dadai", "Medium", "Datei", "Bild"],
    "pi": ["मीडिया", "पटिमा"],
    "pl": ["Plik", "Grafika"],
    "pms": ["Figura", "Immagine"],
    "pnb": ["میڈیا", "تصویر", "فائل"],
    "pnt": ["Εικόνα", "Αρχείον", "Εικόναν", "Μέσον"],
    "ps": ["انځور", "رسنۍ", "دوتنه"],
    "pt": ["Multimédia", "Ficheiro", "Arquivo", "Imagem"],
    "qu": ["Midya", "Imagen", "Rikcha"],
    "rm": ["Multimedia", "Datoteca"],
    "rmy": ["Fişier", "Mediya", "Chitro", "Imagine"],
    "ro": ["Fişier", "Imagine", "Fișier"],
    "roa-rup": ["Fişier", "Imagine", "Fișier"],
    "roa-tara": ["Immagine"],
    "ru": ["Медиа", "Файл", "Изображение"],
    "rue": ["Медіа", "Медиа", "Файл", "Изображение", "Зображення"],
    "rw": ["Dosiye", "Itangazamakuru"],
    "sa": ["चित्रम्", "माध्यमम्", "सञ्चिका", "माध्यम", "चित्रं"],
    "sah": ["Миэдьийэ", "Ойуу", "Билэ", "Изображение"],
    "sat": ["ᱨᱮᱫ", "ᱢᱤᱰᱤᱭᱟ"],
    "sc": ["Immàgini"],
    "scn": ["Immagine", "Mmàggini", "Mèdia"],
    "sd": ["عڪس", "ذريعات", "فائل"],
    "se": ["Fiila"],
    "sg": ["Média", "Fichier"],
    "sh": ["Mediji", "Slika", "Медија", "Datoteka", "Medija", "Слика"],
    "si": ["රූපය", "මාධ්‍යය", "ගොනුව"],
    "sk": ["Súbor", "Obrázok", "Médiá"],
    "sl": ["Slika", "Datoteka"],
    "sq": ["Figura", "Skeda"],
    "sr": ["Датотека", "Medij", "Slika", "Медија", "Datoteka", "Медиј", "Medija", "Слика"],
    "srn": ["Afbeelding", "Gefre"],
    "stq": ["Bielde", "Bild"],
    "su": ["Média", "Gambar"],
    "sv": ["Fil", "Bild"],
    "sw": ["Faili", "Picha"],
    "szl": ["Plik", "Grafika"],
    "ta": ["படிமம்", "ஊடகம்"],
    "tcy": ["ಮಾದ್ಯಮೊ", "ಫೈಲ್"],
    "te": ["ఫైలు", "దస్త్రం", "బొమ్మ", "మీడియా"],
    "tet": ["Imajen", "Arquivo", "Imagem"],
    "tg": ["Акс", "Медиа"],
    "th": ["ไฟล์", "สื่อ", "ภาพ"],
    "ti": ["ፋይል", "ሜድያ"],
    "tk": ["Faýl"],
    "tl": ["Midya", "Talaksan"],
    "tpi": ["Fail"],
    "tr": ["Medya", "Resim", "Dosya", "Ortam"],
    "tt": ["Медиа", "Рәсем", "Файл", "Räsem", "Изображение"],
    "ty": ["Média", "Fichier"],
    "tyv": ["Медиа", "Файл", "Изображение"],
    "udm": ["Медиа", "Файл", "Суред", "Изображение"],
    "ug": ["ۋاسىتە", "ھۆججەت"],
    "uk": ["Медіа", "Медиа", "Файл", "Изображение", "Зображення"],
    "ur": ["میڈیا", "تصویر", "وسیط", "زریعہ", "فائل", "ملف"],
    "uz": ["Mediya", "Tasvir", "Fayl"],
    "vec": ["Immagine", "Imàjine", "Mèdia"],
    "vep": ["Pilt", "Fail"],
    "vi": ["Phương_tiện", "Tập_tin", "Hình", "Tập tin", "Phương tiện"],
    "vls": ["Afbeelding", "Ofbeeldienge"],
    "vo": ["Ragiv", "Magod", "Nünamakanäd"],
    "wa": ["Imådje"],
    "war": ["Medya", "Fayl", "Paypay"],
    "wo": ["Xibaarukaay", "Dencukaay"],
    "wuu": ["文件", "档案", "图像", "媒体"],
    "xal": ["Аһар", "Боомг", "Изображение", "Зург"],
    "xmf": ["მედია", "სურათი", "ფაილი"],
    "yi": ["מעדיע", "תמונה", "טעקע", "בילד"],
    "yo": ["Fáìlì", "Amóhùnmáwòrán", "Àwòrán"],
    "za": ["媒体文件", "文件", "档案", "图像", "媒体"],
    "zea": ["Afbeelding", "Plaetje"],
    "zh": ["媒体文件", "F", "文件", "媒體", "档案", "图像", "圖像", "媒体", "檔案"],
    "zh-classical": ["文件", "媒體", "圖像", "檔案"],
    "zh-min-nan": ["tóng-àn", "文件", "媒體", "Mûi-thé", "圖像", "檔案"],
    "zh-yue": ["檔", "档", "文件", "图", "媒體", "圖", "档案", "图像", "圖像", "媒体", "檔案"],
}

CAT_ALIASES = {
    "ab": ["Категория", "Акатегориа"],
    "ace": ["Kawan", "Kategori"],
    "af": ["Kategorie"],
    "ak": ["Nkyekyem"],
    "als": ["Kategorie"],
    "am": ["መደብ"],
    "an": ["Categoría"],
    "ang": ["Flocc"],
    "ar": ["تصنيف"],
    "arc": ["ܣܕܪܐ"],
    "arz": ["تصنيف"],
    "as": ["CAT", "শ্ৰেণী", "श्रेणी", "শ্রেণী"],
    "ast": ["Categoría"],
    "atj": ["Tipanictawin"],
    "av": ["Категория"],
    "ay": ["Categoría"],
    "az": ["Kateqoriya"],
    "azb": ["بؤلمه"],
    "ba": ["Төркөм", "Категория"],
    "bar": ["Kategorie"],
    "bat-smg": ["Kategorija", "Kateguorėjė"],
    "bcl": ["Kategorya"],
    "be": ["Катэгорыя"],
    "be-x-old": ["Катэгорыя"],
    "bg": ["Категория"],
    "bh": ["श्रेणी"],
    "bjn": ["Tumbung", "Kategori"],
    "bm": ["Catégorie"],
    "bn": ["বিষয়শ্রেণী", "വിഭാഗം"],
    "bpy": ["থাক"],
    "br": ["Rummad"],
    "bs": ["Kategorija"],
    "bug": ["Kategori"],
    "bxr": ["Категори", "Категория"],
    "ca": ["Categoria"],
    "cbk-zam": ["Categoría"],
    "cdo": ["分類"],
    "ce": ["Категори", "Тоба", "Кадегар"],
    "ceb": ["Kategoriya"],
    "ch": ["Katigoria"],
    "ckb": ["پ", "پۆل"],
    "co": ["Categoria"],
    "crh": ["Категория", "Kategoriya"],
    "cs": ["Kategorie"],
    "csb": ["Kategòrëjô"],
    "cu": ["Катигорї", "Категория", "Катигорїꙗ"],
    "cv": ["Категори"],
    "cy": ["Categori"],
    "da": ["Kategori"],
    "de": ["Kategorie"],
    "din": ["Bekätakthook"],
    "diq": ["Kategoriye", "Kategori"],
    "dsb": ["Kategorija"],
    "dty": ["श्रेणी"],
    "dv": ["ޤިސްމު"],
    "el": ["Κατηγορία"],
    "eml": ["Categoria"],
    "eo": ["Kategorio"],
    "es": ["CAT", "Categoría"],
    "et": ["Kategooria"],
    "eu": ["Kategoria"],
    "ext": ["Categoría", "Categoria"],
    "fa": ["رده"],
    "ff": ["Catégorie"],
    "fi": ["Luokka"],
    "fiu-vro": ["Katõgooria"],
    "fo": ["Bólkur"],
    "fr": ["Catégorie"],
    "frp": ["Catègorie"],
    "frr": ["Kategorie"],
    "fur": ["Categorie"],
    "fy": ["Kategory"],
    "ga": ["Rang", "Catagóir"],
    "gag": ["Kategori", "Kategoriya"],
    "gan": ["分類", "分类"],
    "gd": ["Roinn-seòrsa"],
    "gl": ["Categoría"],
    "glk": ["جرگه", "رده"],
    "gn": ["Ñemohenda"],
    "gom": ["वर्ग", "श्रेणी"],
    "gor": ["Dalala"],
    "got": ["𐌷𐌰𐌽𐍃𐌰"],
    "gu": ["શ્રેણી", "CAT", "શ્રે"],
    "gv": ["Ronney"],
    "hak": ["分類"],
    "haw": ["Māhele"],
    "he": ["קטגוריה", "קט"],
    "hi": ["श्र", "श्रेणी"],
    "hif": ["vibhag"],
    "hr": ["CT", "KT", "Kategorija"],
    "hsb": ["Kategorija"],
    "ht": ["Kategori"],
    "hu": ["Kategória"],
    "hy": ["Կատեգորիա"],
    "ia": ["Categoria"],
    "id": ["Kategori"],
    "ie": ["Categorie"],
    "ig": ["Ébéonọr", "Òtù"],
    "ii": ["分类"],
    "ilo": ["Kategoria"],
    "inh": ["ОагӀат"],
    "io": ["Kategorio"],
    "is": ["Flokkur"],
    "it": ["CAT", "Categoria"],
    "ja": ["カテゴリ"],
    "jbo": ["klesi"],
    "jv": ["Kategori"],
    "ka": ["კატეგორია"],
    "kaa": ["Sanat", "Kategoriya", "Санат", "سانات"],
    "kab": ["Taggayt"],
    "kbd": ["Категория", "Категориэ"],
    "kbp": ["Catégorie"],
    "kg": ["Kalasi"],
    "kk": ["Sanat", "Санат", "سانات"],
    "kl": ["Sumut_atassuseq", "Kategori", "Sumut atassuseq"],
    "km": ["ចំនាត់ថ្នាក់ក្រុម", "ចំណាត់ក្រុម", "ចំណាត់ថ្នាក់ក្រុម"],
    "kn": ["ವರ್ಗ"],
    "ko": ["분류"],
    "koi": ["Категория"],
    "krc": ["Категория"],
    "ks": ["زٲژ"],
    "ksh": ["Saachjropp", "Saachjrop", "Katejori", "Kategorie", "Saachjrupp", "Kattejori", "Sachjrop"],
    "ku": ["Kategorî", "پۆل"],
    "kv": ["Категория"],
    "kw": ["Class", "Klass"],
    "ky": ["Категория"],
    "la": ["Categoria"],
    "lad": ["Kateggoría", "Katēggoría", "Categoría"],
    "lb": ["Kategorie"],
    "lbe": ["Категория"],
    "lez": ["Категория"],
    "lfn": ["Categoria"],
    "li": ["Categorie", "Kategorie"],
    "lij": ["Categorîa", "Categoria"],
    "lmo": ["Categuria", "Categoria"],
    "ln": ["Catégorie"],
    "lo": ["ໝວດ"],
    "lrc": ["دأسە"],
    "lt": ["Kategorija"],
    "ltg": ["Kategoreja"],
    "lv": ["Kategorija"],
    "mai": ["CA", "श्रेणी"],
    "map-bms": ["Kategori"],
    "mdf": ["Категорие", "Категория"],
    "mg": ["Sokajy", "Catégorie"],
    "mhr": ["Категория", "Категорий"],
    "min": ["Kategori"],
    "mk": ["Категорија"],
    "ml": ["വിഭാഗം", "വി", "വർഗ്ഗം", "വ"],
    "mn": ["Ангилал"],
    "mr": ["वर्ग"],
    "mrj": ["Категори", "Категория"],
    "ms": ["Kategori"],
    "mt": ["Kategorija"],
    "mwl": ["Catadorie", "Categoria"],
    "my": ["ကဏ္ဍ"],
    "myv": ["Категория"],
    "mzn": ["رج", "رده"],
    "nah": ["Neneuhcāyōtl", "Categoría"],
    "nap": ["Categurìa", "Categoria"],
    "nds": ["Kategorie"],
    "nds-nl": ["Categorie", "Kattegerie", "Kategorie"],
    "ne": ["श्रेणी"],
    "new": ["पुचः"],
    "nl": ["Categorie"],
    "nn": ["Kategori"],
    "no": ["Kategori"],
    "nrm": ["Catégorie"],
    "nso": ["Setensele"],
    "nv": ["Tʼááłáhági_átʼéego", "Tʼááłáhági átʼéego"],
    "oc": ["Categoria"],
    "olo": ["Kategourii"],
    "or": ["ବିଭାଗ", "ଶ୍ରେଣୀ"],
    "os": ["Категори"],
    "pa": ["ਸ਼੍ਰੇਣੀ"],
    "pcd": ["Catégorie"],
    "pdc": ["Abdeeling", "Kategorie"],
    "pfl": ["Kadegorie", "Sachgrubb", "Kategorie"],
    "pi": ["विभाग"],
    "pl": ["Kategoria"],
    "pms": ["Categorìa"],
    "pnb": ["گٹھ"],
    "pnt": ["Κατηγορίαν"],
    "ps": ["وېشنيزه"],
    "pt": ["Categoria"],
    "qu": ["Katiguriya"],
    "rm": ["Categoria"],
    "rmy": ["Shopni"],
    "ro": ["Categorie"],
    "roa-rup": ["Categorie"],
    "roa-tara": ["Categoria"],
    "ru": ["Категория", "К"],
    "rue": ["Категория", "Катеґорія"],
    "rw": ["Ikiciro"],
    "sa": ["वर्गः"],
    "sah": ["Категория"],
    "sat": ["ᱛᱷᱚᱠ"],
    "sc": ["Categoria"],
    "scn": ["Catigurìa"],
    "sd": ["زمرو"],
    "se": ["Kategoriija"],
    "sg": ["Catégorie"],
    "sh": ["Kategorija", "Категорија"],
    "si": ["ප්‍රවර්ගය"],
    "sk": ["Kategória"],
    "sl": ["Kategorija"],
    "sq": ["Kategoria", "Kategori"],
    "sr": ["Kategorija", "Категорија"],
    "srn": ["Categorie", "Guru"],
    "stq": ["Kategorie"],
    "su": ["Kategori"],
    "sv": ["Kategori"],
    "sw": ["Jamii"],
    "szl": ["Kategoryjo", "Kategoria"],
    "ta": ["பகுப்பு"],
    "tcy": ["ವರ್ಗೊ"],
    "te": ["వర్గం"],
    "tet": ["Kategoría", "Kategoria"],
    "tg": ["Гурӯҳ"],
    "th": ["หมวดหมู่"],
    "ti": ["መደብ"],
    "tk": ["Kategoriýa"],
    "tl": ["Kategorya", "Kaurian"],
    "tpi": ["Grup"],
    "tr": ["Kategori", "KAT"],
    "tt": ["Төркем", "Törkem", "Категория"],
    "ty": ["Catégorie"],
    "tyv": ["Аңгылал", "Категория"],
    "udm": ["Категория"],
    "ug": ["تۈر"],
    "uk": ["Категория", "Категорія"],
    "ur": ["زمرہ"],
    "uz": ["Turkum", "Kategoriya"],
    "vec": ["Categoria"],
    "vep": ["Kategorii"],
    "vi": ["Thể_loại", "Thể loại"],
    "vls": ["Categorie"],
    "vo": ["Klad"],
    "wa": ["Categoreye"],
    "war": ["Kaarangay"],
    "wo": ["Wàll", "Catégorie"],
    "wuu": ["分类"],
    "xal": ["Янз", "Әәшл"],
    "xmf": ["კატეგორია"],
    "yi": ["קאטעגאריע", "קאַטעגאָריע"],
    "yo": ["Ẹ̀ka"],
    "za": ["分类"],
    "zea": ["Categorie"],
    "zh": ["分类", "分類", "CAT"],
    "zh-classical": ["分類", "CAT"],
    "zh-min-nan": ["分類", "Lūi-pia̍t"],
    "zh-yue": ["分类", "分類", "类", "類"],
}


IMAGE_EXTENSIONS = ['.jpg', '.png', '.svg', '.gif', '.jpeg', '.tif',
                    '.bmp', '.webp', '.xcf']
VIDEO_EXTENSIONS = ['.ogv', '.webm', '.mpg', '.mpeg']
AUDIO_EXTENSIONS = ['.ogg', '.mp3', '.mid', '.webm', '.flac', '.wav',
                    '.oga']
MEDIA_EXTENSIONS = list(set(IMAGE_EXTENSIONS + VIDEO_EXTENSIONS +
                            AUDIO_EXTENSIONS))
# build regex that checks for all media extensions
EXTEN_REGEX = ('(' +
               '|'.join([e + r'\b' for e in MEDIA_EXTENSIONS])
               + ')').replace('.', r'\.')
# join in the extension regex with one that requiries at least one
# alphanumeric and/or a few special characters before it
EXTEN_PATTERN = re.compile(fr"([\w ',().-]+){EXTEN_REGEX}",
                           flags=re.UNICODE)
ref_singleton = re.compile(r'<ref(\s[^/>]*)?/>', re.M | re.I)
ref_tag = re.compile(r'<ref(\s[^/>]*)?>[\s\S]*?</ref>', re.M | re.I)

# The maximum page size allowed. 309043 correspond
# to the 0.999 quantile. The top 0.001 are usually vandalism or
# data errors, we mark them with num_refs = -1, in order to allow
# further investigaiton.
MAX_PAGE_LENGTH = 309043

def get_article_features(wikitext, wiki_db, level=3):
    """Gather counts of article components directly from wikitext.

    Pros:
    * Much faster than mwparserfromhell (10x speed-up in testing) --
      e.g., 200 µs vs. 2 ms for medium-sized article
    * Can be easily extended to catch edge-cases -- e.g., images added
      via infoboxes/galleries that lack bracket syntax

    Cons/Issues:
    * Misses intra-nested links:
        * e.g. [[File:Image.jpg|Image with a [[caption]]]] only catches
          the File and not the [[caption]]
        * Could be extended by also regexing each link found, which
          should catch almost all
    * Misses references added via templates w/o ref tags -- e.g.,
      shortened-footnote templates.
    * Misses media added via templates / gallery tags that lack brackets


    Parameters
    ----------
    wikitext : str
        Revision text.

    wiki_db : str
        Wikipedia database.

    level : int
        Heading upto this level are considered.

    Returns
    -------
        [(page_length : int, num_refs : int, num_wikilinks : int,
          num_categories : int, num_media : int, num_headings : int)]
    """
    try:
        lang = wiki_db.replace('wiki', '')
        cat_prefixes = CAT_PREFIXES + CAT_ALIASES.get(lang, [])
        med_prefixes = MEDIA_PREFIXES + MEDIA_ALIASES.get(lang, [])
        page_length = len(wikitext)
        if page_length < MAX_PAGE_LENGTH:
            refs = len(ref_singleton.findall(wikitext)) + len(ref_tag.findall(wikitext))
            links = [m.split('|', maxsplit=1)[0]
                     for m in re.findall(r'(?<=\[\[)(.*?)(?=]])', wikitext, flags=re.DOTALL)]
            categories = len([1 for l in links
                              if l.split(':', maxsplit=1)[0] in cat_prefixes])
            media_bra = [l.split(':', maxsplit=1)[1]
                         for l in links if l.split(':', maxsplit=1)[0] in med_prefixes]
            # The media_ext regex is very expensive. Diego assumes that
            # media_ext are unusual. So, first check if there is string
            # matching with the extensions and just in that case I run
            # the regex.
            exts = False
            for m in MEDIA_EXTENSIONS:
                if m in wikitext:
                    exts = True
                    break
            if exts:
                media_ext = [''.join(m).strip()
                             for m in EXTEN_PATTERN.findall(wikitext) if len(m[0]) <= 240]
            else:
                media_ext = []
            media = len(set(media_bra).union(set(media_ext)))
            wikilinks = len(links) - categories - len(media_bra)
            headings = len([1 for l in re.findall('(={2,})(.*?)(={2,})', wikitext) if len(l[0]) <= level])
            return [(page_length, refs, wikilinks, categories, media, headings)]
        else:
            return [(page_length, -1, -1, -1, -1, -1)]
    except Exception as e:
        print(e)
        return [(-2, -2, -2, -2, -2, -2)]


def generate_features(
        spark,
        mediawiki_snapshot,
        start_date,
        end_date,
        projects=None,
        project_namespace_map_table='wmf_raw.mediawiki_project_namespace_map',
        page_table='wmf_raw.mediawiki_page',
        wikitext_table='wmf.mediawiki_wikitext_history'
):
    """
    The quality model requires the following attributes:
    * Quantity:
      * # bytes (page length)
      * # links
      * # of headers (just levels 2 + 3)
      * # of images
    * Accessibility
      * bytes / header
    * Reliability
      * # of references
    * Annotations
      * # of categories

    Not included but considered:
    * Wikidata item completeness
    * alt-text coverage
    * issue templates
    * # of assessments / WikiProjects

    """
    # To be used as: SELECT INLINE(getArticleFeatures(wikitext)) FROM ...
    # The array part is incidental -- no method for inline pure structs...
    spark.udf.register('getArticleFeatures', get_article_features,
                       'ARRAY<STRUCT<page_length:INT, num_refs:INT, ' +
                       'num_wikilinks:INT, num_categories:INT, ' +
                       'num_media:INT, num_headings:INT>>')

    def projects_clause(table):
      return f"AND {table}.wiki_db in {python_to_sql_list(projects)}" \
        if projects else ""

    timestamp_clause = ""
    if start_date or end_date:
      if not start_date:
        start_date = datetime.strptime("20000101", '%Y%m%d')
      elif not end_date:
        end_date = datetime.now()

      timestamp_clause = f"""AND wt.revision_timestamp BETWEEN "{start_date.strftime('%Y-%m-%d')}"
                AND "{end_date.strftime('%Y-%m-%d')}"""

    query = f"""
    WITH wikipedia_projects AS (
        SELECT DISTINCT
          dbname
        FROM {project_namespace_map_table}
        WHERE
          snapshot = '{mediawiki_snapshot}'
          AND hostname LIKE '%.wikipedia%'
    ),
    pages AS (
        SELECT
          wiki_db,
          page_id
        FROM {page_table} p
        INNER JOIN wikipedia_projects wp
          ON (p.wiki_db = wp.dbname)
        WHERE
          snapshot = '{mediawiki_snapshot}'
          AND page_namespace = 0
          AND NOT page_is_redirect
          {projects_clause("p")}
    ),
    features AS (
        SELECT
          wt.wiki_db,
          wt.page_id,
          wt.revision_timestamp,
          wt.revision_id,
          INLINE(getArticleFeatures(revision_text, wt.wiki_db, 3))
        FROM {wikitext_table} wt
        WHERE
          wt.snapshot = '{mediawiki_snapshot}'
          {projects_clause("wt")}
          AND wt.page_namespace = 0
          AND wt.page_redirect_title =''
          AND wt.wiki_db not in ('wikidatawiki', 'testwiki', 'metawiki', 'specieswiki', 'mediawikiwiki')
          AND substr(wt.wiki_db,-4) = 'wiki'
          {timestamp_clause}"
    )
    SELECT
      f.wiki_db,
      f.page_id,
      revision_id,
      revision_timestamp,
      COALESCE(page_length, 0) AS page_length,
      COALESCE(num_refs, 0) AS num_refs,
      COALESCE(num_wikilinks, 0) AS num_wikilinks,
      COALESCE(num_categories, 0) AS num_categories,
      COALESCE(num_media, 0) AS num_media,
      COALESCE(num_headings, 0) AS num_headings
    FROM features f
    INNER JOIN pages p
          ON (f.page_id = p.page_id
              AND f.wiki_db = p.wiki_db)
    """
    return spark.sql(query)

def predict_quality(sqrt_length, num_media, num_headings_per,
                    num_refs_per, num_cats, sqrt_links_per):
    """Predict quality of article."""

    coef_len = 0.395
    coef_med = 0.114
    coef_ref = 0.181
    coef_hea = 0.123
    coef_lin = 0.115
    coef_cat = 0.070
    try:
        pred = ((coef_len * sqrt_length) +
                (coef_med * num_media) +
                (coef_ref * num_refs_per) +
                (coef_hea * num_headings_per) +
                (coef_lin * sqrt_links_per) +
                (coef_cat * num_cats))
        return pred
    except Exception as e:
        print(e)
        return None


def generate_scores(
        spark,
        mediawiki_snapshot,
        wikidata_snapshot,
        projects,
        features_table,
        project_namespace_map_table='wmf_raw.mediawiki_project_namespace_map',
        item_page_link_table='wmf.wikidata_item_page_link',
):
    """Generate article quality scores."""
    # 95% percentile captures top quality but filters out extreme
    # outliers
    qual_pctile = 0.95

    # Determined empirically -- these are straightforward because they
    # are raw counts.
    MIN_MEDIA = 2
    MIN_CATS = 5

    # Determined empirically and equivalent to 10000 characters which is
    # exceeded even in Chinese Wikipedia (12526 characters in 95th
    # percentile). Remember it's a single transformation: sqrt(# bytes
    # in page).
    MIN_SQRTLENGTH = 100

    # Determined empirically -- mostly penalizes bot-driven wikis that
    # have many articles with lede paragraphs so 0 sections. Remember
    # it's a single transformation: # headings / sqrt(# bytes in page).
    # Equivalent to 1 section @ 100 characters; 2 sections at 400
    # characters; 3 sections at 900 characters etc.
    MIN_HEADINGS_PER = 0.1

    # Determined empirically but wikis it affects are mixed bags (some
    # bot-driven wikis have very heavily-referenced short articles).
    # Many wikis are closer to 0.25 - 0.3 range. Remember it's a single
    # transformation: # references / sqrt(# bytes in page). Equivalent
    # to 1 ref @ 45 characters; 2 refs at 180 characters; 3 refs at 400
    # characters etc. Or ~2 references per section taking the
    # min_headings_per logic.
    MIN_REFS_PER = 0.15

    # Determined empirically -- almost all wikis are over 0.1 with CJK
    # getting close to 0.2. Remember it's a double transformation:
    # sqrt(# wikilinks) / sqrt(# bytes in page). Equivalent to 1 link
    # per 100 characters, which is ~1 link per sentence.
    MIN_SQRTLINKS_PER = 0.1

    spark.udf.register('predict_quality', predict_quality,
                       returnType='Float')

    def projects_clause(table):
      return f"AND {table}.wiki_db in {python_to_sql_list(projects)}" \
        if projects else ""

    query = f"""
    WITH wikipedia_projects AS (
        SELECT DISTINCT
          dbname,
          SUBSTR(hostname, 1, LENGTH(hostname)-4) AS project
        FROM {project_namespace_map_table}
        WHERE
          snapshot = '{mediawiki_snapshot}'
          AND hostname LIKE '%wikipedia%'
    ),
    wikidata_ids AS (
        SELECT
          wiki_db,
          page_id,
          item_id
        FROM {item_page_link_table} wd
        INNER JOIN wikipedia_projects wp
          ON (wd.wiki_db = wp.dbname)
        WHERE
          snapshot = '{wikidata_snapshot}'
          AND page_namespace = 0
          {projects_clause("wd")}
    ),
    max_vals AS (
        SELECT
          qf.wiki_db,
          GREATEST({MIN_SQRTLENGTH}, PERCENTILE_APPROX(SQRT(page_length), {qual_pctile})) AS max_length,
          GREATEST({MIN_MEDIA}, PERCENTILE(num_media, {qual_pctile})) AS max_media,
          GREATEST({MIN_HEADINGS_PER}, PERCENTILE_APPROX(num_headings / SQRT(page_length), {qual_pctile})) AS max_headings,
          GREATEST({MIN_REFS_PER}, PERCENTILE_APPROX(num_refs / SQRT(page_length), {qual_pctile})) AS max_refs,
          GREATEST({MIN_CATS}, PERCENTILE(num_categories, {qual_pctile})) AS max_cats,
          GREATEST({MIN_SQRTLINKS_PER}, PERCENTILE_APPROX(SQRT(num_wikilinks)  / SQRT(page_length), {qual_pctile})) AS max_links
        FROM {features_table} qf
        WHERE
          page_length IS NOT NULL
          AND num_media IS NOT NULL
          AND num_headings IS NOT NULL
          AND num_refs IS NOT NULL
          AND num_categories IS NOT NULL
          AND num_wikilinks IS NOT NULL
        GROUP BY
          qf.wiki_db
    ),
    qual_features_trimmed AS (
        SELECT
          qf.wiki_db,
          qf.page_id,
          qf.revision_id,
          qf.revision_timestamp,
          COALESCE(LEAST(SQRT(page_length), max_length), 0) / max_length AS len_x,
          COALESCE(LEAST(num_media, max_media), 0) / max_media AS media_x,
          COALESCE(LEAST(num_headings / SQRT(page_length), max_headings), 0) / max_headings AS headings_x,
          COALESCE(LEAST(num_refs / SQRT(page_length), max_refs), 0) / max_refs AS refs_x,
          COALESCE(LEAST(num_categories, max_cats), 0) / max_cats AS cats_x,
          COALESCE(LEAST(SQRT(num_wikilinks) / SQRT(page_length), max_links), 0) / max_links AS links_x
        FROM {features_table} qf
        INNER JOIN max_vals mv
          ON (qf.wiki_db = mv.wiki_db)
    ),
    qual_predictions AS (
        SELECT
          wiki_db,
          page_id,
          revision_id,
          revision_timestamp,
          COALESCE(predict_quality(len_x, media_x, headings_x, refs_x, cats_x, links_x), 0) AS pred_qual
        FROM qual_features_trimmed
    )
    SELECT DISTINCT
      qp.wiki_db,
      qp.page_id,
      wd.item_id,
      qp.revision_id,
      qp.revision_timestamp,
      COALESCE(pred_qual, 0) as pred_qual
    FROM qual_predictions qp
    INNER JOIN wikidata_ids wd
      ON (qp.wiki_db = wd.wiki_db
          AND qp.page_id = wd.page_id)
    """
    return spark.sql(query)
