# The script generates development data.

# Not all tables needed for development are created here. Some of the
# tables are created using this script
# https://gitlab.wikimedia.org/repos/research/knowledge-gaps/-/blob/main/knowledge_gaps/config/development.sql.
# Since knowledge-gaps depends on article-quality, there needs to be
# some coupling between these two repositories and their data. That's
# why you need to run the above script first, and then run this one.


import sys
import argparse

from pyspark.sql import SparkSession

from article_quality.util import python_to_sql_list


def execute_query(spark, query):
    """Utility function that prints the query and shows the execution
    result.

    Parameters
    ----------
    spark : SparkSession

    query: str
    """
    print(query)
    spark.sql(query).show(5)


def generate_data_for_mediawiki_project_namespace(
        spark, mediawiki_snapshot, database_name, table_prefix
):
    """Generate data for the mediawiki_project_namespace.

    Parameters
    ----------
    spark : SparkSession

    mediawiki_snapshot: str
        MediaWiki snapshot in the format '2022-01'.

    database_name : str
        The database used for storing data. It is also the name of the
        database used for retrieving data.

    table_prefix : str
        Table name prefix used for fetching data.
    """

    execute_query(spark, f"""
        DROP TABLE IF EXISTS
        {database_name}.{table_prefix}mediawiki_project_namespace_map
    """)
    execute_query(spark, f"""
        CREATE TABLE {database_name}.{table_prefix}mediawiki_project_namespace_map AS
        SELECT *
        FROM wmf_raw.mediawiki_project_namespace_map
        WHERE snapshot = "{mediawiki_snapshot}"
    """)
    execute_query(spark, f"""
        SELECT COUNT(1)
        FROM {database_name}.{table_prefix}mediawiki_project_namespace_map
    """)


def generate_data_for_mediawiki_wiki_history(
        spark, mediawiki_snapshot, wikis, database_name, table_prefix
):
    """Genreate data for the mediawiki_wiki_history table.

    Parameters
    ----------
    spark : SparkSession

    mediawiki_snapshot: str
        MediaWiki snapshot in the format '2022-01'.

    wikis: list[str]
        A list of wiki names, e.g. ['enwiki', 'uzwiki'].

    database_name : str
        The database used for storing data. In development mode, it is
        also the name of the database used for retrieving data.

    table_prefix : str
        Table name prefix used for fetching data.
    """

    execute_query(spark, f"""
        DROP TABLE IF EXISTS
        {database_name}.{table_prefix}mediawiki_wikitext_history """)

    wikis_clause = f"AND mwh.wiki_db in {python_to_sql_list(wikis)}" \
        if wikis else ""
    execute_query(spark, f"""
        CREATE TABLE {database_name}.{table_prefix}mediawiki_wikitext_history AS
        SELECT mwh.*
        FROM wmf.mediawiki_wikitext_history mwh
        INNER JOIN
            (SELECT DISTINCT page_id, wiki_db
             FROM {database_name}.{table_prefix}mediawiki_page_history)
            mph
        ON mwh.page_id = mph.page_id
            AND mwh.wiki_db = mph.wiki_db
        WHERE mwh.snapshot = "{mediawiki_snapshot}"
            AND mwh.page_namespace = 0
            {wikis_clause}
        LIMIT 50000""")

    execute_query(spark, f"""
        SELECT COUNT(1)
        FROM {database_name}.{table_prefix}mediawiki_wikitext_history""")


def generate_data_for_wikidata_item_page_link(
        spark, wikidata_snapshot, wikis, database_name, table_prefix
):
    """Generate data for wikidata_item_page_link table.

    Parameters
    ----------
    spark : SparkSession

    wikidata_snapshot: str
        Wikidata snapshot in the format '2022-01-24'.

    wikis: list[str]
        A list of wiki names, e.g. ['enwiki', 'uzwiki'].

    database_name : str
        The database used for storing data. In development mode, it is
        also the name of the database used for retrieving data.

    table_prefix : str
        Table name prefix used for fetching data.

    """
    execute_query(spark, f"""
        DROP TABLE IF EXISTS
        {database_name}.{table_prefix}wikidata_item_page_link""")

    wikis_clause = f"AND wipl.wiki_db in {python_to_sql_list(wikis)}" \
        if wikis else ""
    execute_query(spark, f"""
        CREATE TABLE {database_name}.{table_prefix}wikidata_item_page_link AS
        SELECT wipl.*
        FROM {database_name}.{table_prefix}mediawiki_page_history mph
        INNER JOIN wmf.wikidata_item_page_link wipl
        ON wipl.page_id = mph.page_id
            AND wipl.wiki_db = mph.wiki_db
        WHERE wipl.snapshot = "{wikidata_snapshot}"
            AND wipl.page_namespace = 0
            {wikis_clause}
        LIMIT 50000
    """)

    execute_query(spark, f"""
        SELECT COUNT(1)
        FROM {database_name}.{table_prefix}wikidata_item_page_link""")


def generate_data(spark, mediawiki_snapshot, wikidata_snapshot,
                  wikis, database_name, table_prefix):
    """Generate development data.

    Parameters
    ----------
    spark : SparkSession

    mediawiki_snapshot: str
        Wikidata snapshot in the format '2022-01'.

    wikidata_snapshot: str
        Wikidata snapshot in the format '2022-01-24'.

    wikis: list[str]
        A list of wiki names, e.g. ['enwiki', 'uzwiki'].

    database_name : str
        The database used for storing data. In development mode, it is
        also the name of the database used for retrieving data.

    table_prefix : str
        Table name prefix used for fetching data.
    """

    generate_data_for_mediawiki_project_namespace(
        spark, mediawiki_snapshot, database_name, table_prefix
    )
    generate_data_for_mediawiki_wiki_history(
        spark, mediawiki_snapshot, wikis, database_name, table_prefix
    )
    generate_data_for_wikidata_item_page_link(
        spark, wikidata_snapshot, wikis, database_name, table_prefix
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mediawiki_snapshot',
                        help='Mediawiki snapshot, e.g. 2022-05.')
    parser.add_argument('--wikidata_snapshot',
                        help='Wikidata snapshot, e.g. 2022-05-23.')
    parser.add_argument('--wikis',
                        type=lambda l: l.split(','),
                        help='Comma separated list of wikis, e.g. '
                        '"cawiki,enwiki". If not supplied all projects '
                        'will be used.')
    parser.add_argument('--database_name',
                        default='knowledge_gaps',
                        help='In production mode, data will be saved in '
                        'this database. In development mode, in '
                        'addition to being saved, additional data will '
                        'also be retrieved from this database.')
    parser.add_argument('--table_prefix',
                        default="",
                        help='In production mode, data will be saved in '
                        'table names prefixed with this prefix. In '
                        'development mode, the prefix is used for both '
                        'querying data from tables and saving data in '
                        'tables with this prefix.')

    args = parser.parse_args()
    spark = SparkSession.builder.getOrCreate()
    generate_data(
        spark,
        args.mediawiki_snapshot,
        args.wikidata_snapshot,
        args.wikis,
        args.database_name,
        args.table_prefix
    )


if __name__ == '__main__':
    sys.exit(main())

