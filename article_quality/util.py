def python_to_sql_list(l):
    """Convert a non-empty `list` into a valid SQL list that can be used in
    queries.

    Examples:
    [] => '()'
    ['enwiki'] => '("enwiki")'
    ['enwiki',] => '("enwiki")'
    ['enwiki', "uzwiki"] => '("enwiki", "uzwiki")'
    [2020, 2021] => '(2020, 2021)'

    Parameters
    ----------
    l : List[Union[int, str]]

    Returns
    -------
    str
    """
    if not l:
        return '()'
    if isinstance(l[0], int):
        return '(' + ', '.join([str(x) for x in l]) + ')'
    return '("' + '", "'.join(l) + '")'


def save_df(df, table=None, filename=None):
    """Partition `df` by `wiki_db` and save it to the temporary table
    `table`, if it's supplied. If `filename` is supplied, save the
    dataframe to `filename` too.

    Parameters
    ----------
    df : pyspark.sql.dataframe.DataFrame

    table : str
        Name of the table where `df` will be saved.

    filename: str|None
        Name of the file where `df` will be saved. If not supplied, the
        data frame won't be saved to file.

    """
    if filename:
        df = df.cache()
        df.write.mode("overwrite").partitionBy('wiki_db').parquet(filename)
    if table:
        df.createOrReplaceTempView(table)
