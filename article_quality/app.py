import argparse
from datetime import datetime

from pyspark.sql import functions as F
from pyspark.sql import SparkSession

from article_quality import quality_model

def common_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mediawiki_snapshot',
                        required=True,
                        help='Mediawiki snapshot, e.g. 2022-05.')
    parser.add_argument('--projects',
                        type=lambda l: l.split(','),
                        help='Comma separated list of projects, e.g. '
                        '"cawiki,enwiki". If not supplied all projects '
                        'will be used.')
    parser.add_argument('--mode',
                        choices=['production', 'development'],
                        default='development',
                        help='Mode to run the script in. In production '
                        'mode data will be generated using production '
                        'tables, and in development mode smaller tables '
                        'will be used to generate data.')
    parser.add_argument('--dev_database_name',
                        help='In development mode, this database used'
                        'to retrieve smaller datasets.')
    parser.add_argument('--dev_table_prefix',
                        default='',
                        help='In development mode, the prefix can'
                        'be used namespace input tables.')
    parser.add_argument('--output_table',
                        required=True,
                        help='The hive output tabale.')
    return parser



def features():
    parser = common_args()

    parser.add_argument('--write_mode',
                    required=True,
                    choices=['append', 'overwrite'],
                    help="""Write mode, use append for an incremental dataset""")

    parser.add_argument('--start_date',
                        type=lambda d: datetime.strptime(d, '%Y%m%d'),
                        help="""Start date in the following format:
                            YYYYMMDD, e.g. 20220131.""")
    parser.add_argument('--end_date',
                        type=lambda d: datetime.strptime(d, '%Y%m%d'),
                        help="""End date in the following format:
                            YYYYMMDD, e.g. 20220131. """)

    parser.add_argument('--coalesce_features',
                        default=5,
                        type=int,
                        help='The number of files for the features dataset, per time_partition')

    args = parser.parse_args()

    if args.mode == 'development':
        project_namespace_map_table = f'{args.dev_database_name}.{args.dev_table_prefix}mediawiki_project_namespace_map'
        page_table = f'{args.dev_database_name}.{args.dev_table_prefix}mediawiki_page'
        wikitext_table = f'{args.dev_database_name}.{args.dev_table_prefix}mediawiki_wikitext_history'

    else:
        project_namespace_map_table = 'wmf_raw.mediawiki_project_namespace_map'
        page_table = 'wmf_raw.mediawiki_page'
        wikitext_table = 'wmf.mediawiki_wikitext_history'

    spark = SparkSession\
        .builder\
        .getOrCreate()

    features = quality_model.generate_features(
        spark, args.mediawiki_snapshot, args.start_date, args.end_date, args.projects,
        project_namespace_map_table, page_table, wikitext_table
    )

    time_partition_format = 'yyyy-MM'
    (features
        .withColumn("time_partition", F.date_format('revision_timestamp', time_partition_format))
        .repartition(args.coalesce_features, "time_partition")
        .write
        .partitionBy(["time_partition"])
        .mode(args.write_mode)
        .format('hive')
        .saveAsTable(args.output_table))

def scores():
    parser = common_args()

    parser.add_argument('--wikidata_snapshot',
                        required=True,
                        help='Wikidata snapshot, e.g. 2022-05-23.')

    parser.add_argument('--features_table',
                        required=True,
                        help='Article features hive table')

    parser.add_argument('--coalesce_scores',
                        default=200,
                        type=int,
                        help='The number of files for the scores dataset')

    args = parser.parse_args()

    if args.mode == 'development':
        project_namespace_map_table = f'{args.dev_database_name}.{args.dev_table_prefix}mediawiki_project_namespace_map'
        item_page_link_table = f'{args.dev_database_name}.{args.dev_table_prefix}wikidata_item_page_link'
    else:
        project_namespace_map_table = 'wmf_raw.mediawiki_project_namespace_map'
        item_page_link_table = 'wmf.wikidata_item_page_link'

    spark = SparkSession\
        .builder\
        .getOrCreate()

    scores = quality_model.generate_scores(
        spark, args.mediawiki_snapshot, args.wikidata_snapshot, args.projects, args.features_table, project_namespace_map_table,
        item_page_link_table
    )

    (scores
        .coalesce(args.coalesce_scores)
        .write
        .mode('overwrite')
        .saveAsTable(args.output_table))

