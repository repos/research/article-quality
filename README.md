# Article Quality

This repo is archived.
Moved to [research-datasets](https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/4309d8c27b5f7583275db6e30dc48313974676d3/src/research_datasets/article_quality/pipeline.py)

Language-agnostic quality model for articles


## Conda
Create an environment: `conda env create -f environment.yml`.

## Testing
A [CI
Pipeline](https://gitlab.wikimedia.org/repos/research/article-quality/-/pipelines)
is triggered after each `push` and merge request.

Under the hood we use [tox](https://pypi.org/project/tox/) for test
automation. To run tests locally, first install `tox` with:
```
pip install tox
```

Then

- run unit tests with:
```
tox -e test
```

- lint the code base with:
```
tox -e flake8
```

- run compile time typechecks ([mypy](http://mypy-lang.org/)) with
```
tox -e mypy
```

## Running locally
First activate your Conda environment, install the current package, and
pack the environment like so:

```bash
pip install .
conda pack --ignore-editable-packages -o environment.tar.gz
```

The scripts run in one of two modes: `production` or `development`. In
order to run in the development mode, we need to generate data first
(skip this step if you're going to run the script in the production
mode).

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
    spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --executor-memory 16G \
    --archives environment.tar.gz#env \
    article_quality/generate_development_data.py \
    --mediawiki_snapshot 2023-03 \
    --wikidata_snapshot 2023-03-13 \
    --wikis 'frwiki,uzwiki' \
    --database_name=article_quality_dev \
    --table_prefix=dev_
```

As a side note, you can find the latest MW and Wikidata snapshots like
so:
```bash
hadoop fs -ls -C /wmf/data/wmf/mediawiki/page_history | tail -n 1
hadoop fs -ls -C /wmf/data/wmf/wikidata/item_page_link | tail -n 1
```


Now, generate article features like so:

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --conf spark.sql.shuffle.partitions=8000 \
    --driver-memory 4G \
    --executor-memory 16G \
    --archives environment.tar.gz#env \
    $(which features.py) \
    --mediawiki_snapshot 2023-03 \
    --mode development \
    --projects "enwiki,uzwiki" \
    --dev_database_name $(whoami) \
    --dev_table_prefix dev_ \
    --output_table $(whoami).features \
    --start_date 20100101 \
    --end_date 20100131 \
```

Now, generate article quality scores like so:


```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --executor-cores 4 \
    --executor-memory 16G \
    --archives environment.tar.gz#env \
    $(which scores.py) \
    --mediawiki_snapshot 2023-03 \
    --wikidata_snapshot 2023-03-06 \
    --mode development \
    --projects "enwiki,uzwiki" \
    --dev_database_name $(whoami) \
    --dev_table_prefix dev_ \
    --output_table $(whoami).scores \
```


You can also get the results as a dataframe like so (assuming your spark
session is `spark`, and mediawiki and wikidata snapshots are "2022-04"
and "2022-03-28" respectively):

```
from article_quality import quality_model
start_date = datetime.strptime("20220101", '%Y%m%d')
end_date = datetime.strptime("20220301", '%Y%m%d')

features = quality_model.generate_features(
        spark, args.mediawiki_snapshot, args.start_date, args.end_date, args.projects,
        project_namespace_map_table, page_table, wikitext_table
    )
df = get_quality_scores(spark, '2022-04, '2022-03-28', start_date, end_date,
                        ['enwiki'], 'article_quality.features')
```

## Running with airflow

The article features are generated as an incremental dataset using an airflow [DAG](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/research/dags/article_quality_dag.py). The data is stored in the research data folder on hdfs (`/wmf/data/research/article/`).

An external hive table is created with
```
CREATE EXTERNAL TABLE research.article_features (
    page_id BIGINT,
    revision_id BIGINT,
    revision_timestamp STRING,
    page_length INT,
    num_refs INT,
    num_wikilinks INT,
    num_categories INT,
    num_media INT,
    num_headings INT,
    wiki_db STRING
)
PARTITIONED BY (time_partition STRING)
STORED AS PARQUET
LOCATION '/wmf/data/research/article/features/';
```

If there is already data in the location, update the hive metadata with

```
Msck repair table research.article_features;
```


## Versioning

The `article-quality` module, and its conda distribution, is versioned
according to a [semver](https://semver.org/) scheme.

To help propagate version bumps across multiple file locations
(`setup.py`, `Makefile`), a
[bump2version](https://github.com/c4urself/bump2version) config is
provided in `.bumpversion.cfg`.

For example, the following command
```
bump2version patch
```
will increase the patch version in `setup.py` and `Makefile`.
Changes will need to be manually committed to git.

