pytest==6.2.2
pytest-spark==0.6.0
pytest-cov==2.10.1
mypy==0.941
mypy-extensions==0.4.3
typed-ast==1.5.2
typing-extensions==4.1.1
wheel==0.37.1
flake8==4.0.1

