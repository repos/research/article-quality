PROJECT_NAME := article-quality
# Do not modify PROJECT_VERSION manually. Use bump2version instead.
PROJECT_VERSION := 0.0.3
CONDA_DIST := ./dist/${PROJECT_NAME}-${PROJECT_VERSION}.conda-${GIT_BRANCH}-${GIT_COMMIT_HASH}
